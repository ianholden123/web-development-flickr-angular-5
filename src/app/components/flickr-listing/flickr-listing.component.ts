import { Component, OnInit } from '@angular/core';
import { FlickrService } from '../../services/flickr.service';

@Component({
  selector: 'app-flickr-listing',
  templateUrl: './flickr-listing.component.html',
  styleUrls: ['./flickr-listing.component.scss'],
})
export class FlickrListingComponent implements OnInit {

  public flickrPhotos: Object[] = [];
  public lastSearchStr = '';
  public searchStr = '';
  public debounceSpinner = false;

  constructor(
    private flickr: FlickrService,
  ) { }

  ngOnInit() {
    // Start listening to the scroll event to know when to trigger infinite scroll
    const performNewSearch = () => this.newSearch(this.lastSearchStr, true);
    const debounceNewSearch = this.debounced(200, performNewSearch);

    window.onscroll = () => {
      if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight) {
        debounceNewSearch();
      }
    };

    // Trigger the first image load
    this.lastSearchStr = 'landscape, photography, scenery';
    this.newSearch(this.lastSearchStr);
  }

  /**
   * A helper function to call the given callback function once no matter how many
   * times it is triggered within the given delay period.
   * @param delay Set the delay timeout period
   * @param fn Set the callback function
   */
  private debounced(delay, fn) {
    let timerId;
    return function (...args) {
      if (timerId) {
        clearTimeout(timerId);
      }
      timerId = setTimeout(
        () => {
          fn(...args);
          timerId = null;
        },
        delay,
      );
    };
  }

  /**
   * Perform a new search for flickr images.
   * @param searchStr A list of tags to search for. Accepts a comma-separated string.
   * @param append A flag to determine whether we should append to the flickrPhotos variable.
   */
  public newSearch(tags: string, append: boolean = false): void {
    this.debounceSpinner = true;
    this.lastSearchStr = tags;

    const tagsArr = tags.split(',');

    this.flickr.getPhotos(tagsArr).subscribe(
      (photos: any) => {
        if (photos.items) {
          if (append) {
            this.flickrPhotos = this.flickrPhotos.concat(photos.items);
          } else {
            this.flickrPhotos = photos.items;
          }
          this.debounceSpinner = false;
        } else {
          console.error('Could not find images in response');
        }
      },
      (error) => {
        console.error(error);
      },
    );
  }
}
