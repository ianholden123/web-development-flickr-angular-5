import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlickrListingComponent } from './flickr-listing.component';

describe('FlickrListingComponent', () => {
  let component: FlickrListingComponent;
  let fixture: ComponentFixture<FlickrListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FlickrListingComponent],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlickrListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
