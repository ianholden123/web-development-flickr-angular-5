import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { FlickrListingComponent } from './components/flickr-listing/flickr-listing.component';
import { FlickrService } from './services/flickr.service';
import {
  FlickrNameFormatPipe,
  FlickrTagsFormatPipe,
  FlickrDescriptionFormatPipe,
 } from './pipes/flickr-format.pipe';

const appRoutes: Routes = [
  { path: '', component: FlickrListingComponent },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    FlickrListingComponent,
    FlickrNameFormatPipe,
    FlickrTagsFormatPipe,
    FlickrDescriptionFormatPipe,
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false },
    ),
    BrowserModule,
    HttpClientModule,
    HttpClientJsonpModule,
    FormsModule,
  ],
  providers: [
    FlickrService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
