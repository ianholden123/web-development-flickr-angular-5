import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FlickrService {

  private apiEndpoint = 'https://api.flickr.com/services';
  private accountLink = 'https://www.flickr.com/photos';

  constructor(
    private http: HttpClient,
  ) { }

  /**
   * Send a GET request to get photos from Flickr API
   * @param tags Specify a list of tags that the images must contain before returning
   */
  public getPhotos(tags?: string[]): Observable<Object> {
    let tagUrl = '';
    if (tags) {
      tagUrl = `&tags=${tags.join(',')}`;
    }

    const getPhotoEndpoint = `${this.apiEndpoint}/feeds/photos_public.gne?format=json&jsoncallback=?${tagUrl}`;
    return this.http.jsonp(getPhotoEndpoint, 'jsoncallback');
  }

  /**
   * Get the account URL for a given Flickr user account
   * @param accountId The account ID of the user whose Flickr profile link we are getting
   */
  public getAccountLink(accountId: string) {
    return `${this.accountLink}/${accountId}`;
  }

}
