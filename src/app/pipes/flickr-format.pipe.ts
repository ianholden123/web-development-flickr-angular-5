import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'flickrNameFormat',
})
export class FlickrNameFormatPipe implements PipeTransform {

  transform(str: string): string {
    let newStr = str;

    // If we find 'nobody@flickr.com' in the string, remove it.
    if (str.indexOf('nobody@flickr.com') !== -1) {
      newStr = newStr.replace('nobody@flickr.com', '');

      // If we find parentheses and quote marks around string, remove them.
      if (
        newStr.substr(0, 3) === ' ("' &&
        newStr.substr(newStr.length - 2, newStr.length) === '")'
      ) {
        newStr = newStr.substr(3, newStr.length - 5);
      }
    }

    return newStr;
  }

}

@Pipe({
  name: 'flickrDescriptionFormat',
})
export class FlickrDescriptionFormatPipe implements PipeTransform {

  transform(str: string, removeHtml?: boolean): string {
    let newStr = str;

    if (removeHtml) {
      newStr = newStr.replace(/(<([^>]+)>)/ig, '');
    }

    newStr = decodeURIComponent(newStr);

    return newStr;
  }
}

@Pipe({
  name: 'flickrTagsFormat',
})
export class FlickrTagsFormatPipe implements PipeTransform {

  transform(str: string): string {
    return str.replace(/ /g, ', ');
  }
}
