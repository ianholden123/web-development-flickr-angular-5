# Developer Flickr Task

## Notes from Developer
Hello, this is my implementation of the Web Development Flickr Task. In the code below, you should find that I have completed the following:

- Executed the task using Angular 5.
- Styled the project using Sass.
- Implemented tslint-config-airbnb (AirBnB's linting standard).
- Added advanced functionality such as **Infinite scrolling** and **Image search**.
- Created a loading spinner to highlight when images are being loaded in the background.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `npm install` to install all project dependencies and then `ng serve` to start a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
